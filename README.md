# README #

Очень нужное README

### Цель данного проекта ###

* Разработать совместный продукт на Yii2
* Изучить Yii2
* Изучить командную работу в bitbucket

### Что нужно для работы ###

* [Yii2](http://www.yiiframework.com/doc-2.0/guide-start-installation.html#installing-via-composer)
* [Postgres 9.6](https://www.postgresql.org/download/)
* Git

### Порядок настройки локально ####

1. Ставим composer.
1. Ставим стек lamp^ php7, PostgreSQL 9.6.
1. Ставим Git.
1. Ставим Phpstorm.
1. Ставим Xdebug.
1. [Читаем документацию про установку.](http://www.yiiframework.com/doc-2.0/guide-start-installation.html#installing-via-composer)
1. Ставим "fxp/composer-asset-plugin:^1.3.1"
1. Делаем git clone _REPOSITORY_URL_
1. Выполняем composer install
1. [Читаем документацию про миграции.](http://www.yiiframework.com/doc-2.0/guide-db-migrations.html)
1. Применяем миграции
1. Проверяем, что Xdebug работает.
...
1. PROFIT!!!!11

### Соглашения ###

1. Все библиотеки, расширения ставим через composer.
1. Тот кто добавляет новый пакет через composer пишет:

        composer require _PACKAGE_NAME_

    остальные участники:

        composer install

    Так делаем для поддержания папки vendor в одинаковом для всех состоянии.

1. Все изменения структуры БД делаем через миграцию.
1. Миграция, которая уже находится в репозитории, изменению не подлежит. Если нужно вносить изменения, то создаем новую миграцию.
1. В миграциях используем safeUp/safeDown.
1. Функционал, который предстоит обсуждать - разрабатываем в отдельной ветке.
1. Слили свою ветку с master - убрали свою ветку.

BASED on Yii2 basic template
