<?php

namespace app\models;

use Yii;
use yii\base\Model;

class RegistrationForm extends Model
{
    public $email;
    public $password;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // email and password are both required
            [['email', 'password'], 'required'],
            // email must be a unique
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.'],
            // the password must not exceed 64 characters in length
            ['password', 'string', 'length' => [8, 64]],
        ];
    }

    /**
     * Registers a new user, adding it to the database.
     *
     * @return User|false.
     */
    public function registration()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = new User();
        $user->email = $this->email;
        $user->password = $this->password;
        return $user->save() ? $user : false;
    }
}
